resource "google_compute_instance" "instance" {
  project       = "${var.project_name}"
  count         = "${var.count}"
  name          = "${element(var.name, count.index)}-${replace(element(var.zones, count.index), "/^(..).*-(.).*(.)-.*(.)$/", "$1$2$3$4")}-${count.index + 1}"
  zone          = "${element(var.zones, count.index)}"
  machine_type  = "${element(var.type, count.index)}"
  can_ip_forward = "${var.ipforward}"

  tags          = ["${var.tags}"]

  boot_disk {
    initialize_params {
        image       = "${element(var.disk_image, count.index)}"
        size        = "${element(var.disk_size, count.index)}"
        type        = "${element(var.disk_type, count.index)}"
    }
    auto_delete = "${element(var.disk_autodelete, count.index)}"
  }

  network_interface {
    network = "${var.network}"
    subnetwork_project = "${var.subnetwork_project}"
    subnetwork  = "${var.subnetwork}"
    address     = "${length(var.address) > 0 ? element(concat(var.address, list("")), count.index) : ""}"

    access_config {
      // Ephemeral IP
        nat_ip = "${length(var.external_ip) > 0 ? element(concat(var.external_ip, list("")), count.index) : ""}"
    }
  }

  service_account {
    email  = "${var.serviceaccount}"
    scopes = ["${concat(var.serviceaccount_scopes,var.extra_scopes)}"]
  }

  labels = "${var.labels}"

  metadata = "${merge(var.instance_metadata, map("startup-script-url", var.startup_script_url))}"

}

output "uri" {
  value = "${google_compute_instance.instance.*.self_link}"
}

output "self_link" {
  value = ["${google_compute_instance.instance.*.self_link}"]
}

output "private_ip" {
  value = "${google_compute_instance.instance.*.network_ip.0.address}"
}

output "public_ip" {
  value = "${google_compute_instance.instance.*.network_ip.0.access_config.0.assigned_nat_ip}"
}

output "name" {
  value = "${google_compute_instance.instance.*.name}"
}

output "count" {
  value = "${google_compute_instance.instance.count}"
}

output "zone" {
  value = "${google_compute_instance.instance.*.zone}"
}

variable project_name       {}
variable count              {}
variable zones              { type = "list" }
variable tags               { type = "list" default = [] }
variable disk_image         { type = "list" default = [""] }
variable disk_size          { type = "list" default = ["10"] }
variable disk_type          { type = "list" default = ["pd-standard"] }
variable disk_autodelete    { type = "list" default = ["true"] }
variable network            { default = "" }
variable subnetwork         { default = "" }
variable subnetwork_project { default = ""}
variable address            { type = "list" default = [] }
variable external_ip             { type = "list" default = [] }
variable serviceaccount     { default = "" }
variable serviceaccount_scopes { type = "list" default = [
"https://www.googleapis.com/auth/compute",
"https://www.googleapis.com/auth/logging.write",
"https://www.googleapis.com/auth/monitoring.write",
"https://www.googleapis.com/auth/devstorage.full_control"
] 
}
variable extra_scopes       { type = "list" default = [] }
variable name               { type = "list" }
variable type               { type = "list" }
variable ipforward          { default = "false" }
variable startup_script_url { default = "gs://cbsi-ops/instance-startup-scripts/null" }
variable "instance_metadata" {
  type = "map"
  default = {}
}
variable "labels" {
  type = "map"
  default = {}
}
